require("dotenv").config();
const express = require("express");
const cors = require("cors");
const path = require("path");
const bodyParser = require("body-parser");
const database = require("./src/models/db");

// init express
const app = express();

// set up mysql connection
database
  .authenticate()
  .then(() => console.log("Database is working perfectly ..."))
  .catch((err) => console.log("app.js error" + err));

app.disable("x-powered-by");
// access cors (temporary)
app.use(cors());

// parse application/x-www-form-urlencoded & application/json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// use routes
app.use(
  "/api/v1",
  require("./src/routes/products.route"),
  require("./src/routes/user.route"),
  require("./src/routes/role.route"),
  require("./src/routes/upload.route"),
  require("./src/routes/pilot.route"),
  require("./src/routes/orders.route"),
);

// use static
app.use(express.static(path.resolve(__dirname, "./build")));

app.get('*', (req,res) =>{
  res.sendFile(path.resolve(__dirname,'./build/index.html'));
});

// handle 404 (user errors)
app.use((req, res, next) => {
  res.status(404).send(`<h2>Oops 404</h2> <h3>We think you are lost!</h3>`);
});

// start listening
app.listen(process.env.PORT, () =>
  console.log(`app started on ${process.env.PORT}`)
);
