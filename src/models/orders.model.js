const Sequelize = require("sequelize");
const database = require("./db");

const Orders = database.define("orders", {
  orderId: {
    type: Sequelize.INTEGER,
    autoIncrement:true,
    primaryKey: true,
    allowNull: false,
  },
  products: {
    allowNull: false,
    type: Sequelize.STRING,
  },
  userId: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  orderSum: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  notes: {
    type: Sequelize.TEXT,
  },
  status: {
    allowNull: false,
    type: Sequelize.STRING,
  },
});

module.exports = Orders;
