const User = require("./user.model");
const ProductsEN = require("./products.model");
const Roles = require("./roles.model");
const Orders = require("./orders.model");
const Pilot = require("./pilot.model");

const models = {
  User,
  ProductsEN,
  Roles,
  Orders,
  Pilot
};

User.belongsTo(Roles, {
  foreignKey: "roleId",
});
Roles.hasMany(User, {
  foreignKey: "roleId",
});

module.exports = models;
