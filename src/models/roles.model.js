const Sequelize = require("sequelize");
const database = require("./db");

const Roles = database.define("roles", {
  roleId: {
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
  roleName: {
    type: Sequelize.STRING,
  },
  rolePriority: {
    type: Sequelize.INTEGER,
  },
  createdAt: {
    type: Sequelize.DATE,
  },
  updatedAt: {
    type: Sequelize.DATE,
  },
  deletedAt: {
    type: Sequelize.DATE,
  },
});

module.exports = Roles;
