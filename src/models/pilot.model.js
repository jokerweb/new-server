const Sequelize = require("sequelize");
const database = require("./db");

const Pilot = database.define(
  "pilot",
  {
    Blacklist: {
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    DATEN: {
      type: Sequelize.DATE,
    },
    DISTANCE: {
      type: Sequelize.INTEGER,
    },
    LAT_FROM: {
      type: Sequelize.FLOAT,
    },
    LAT_TO: {
      type: Sequelize.INTEGER,
    },
    LON_FROM: {
      type: Sequelize.FLOAT,
    },
    LON_TO: {
      type: Sequelize.INTEGER,
    },
    ORDERID: {
      type: Sequelize.INTEGER,
    },
    NAMEHS: {
      type: Sequelize.STRING,
    },
    NAMEKV: {
      type: Sequelize.STRING,
    },
    NAMEST: {
      type: Sequelize.STRING,
    },
    PRICEALL: {
      type: Sequelize.INTEGER,
    },
    SIMNUM: {
      type: Sequelize.INTEGER,
    },
    TRAVELTIME: {
      type: Sequelize.INTEGER,
    },
  },
  {
    freezeTableName: true,
  }
);

module.exports = Pilot;
