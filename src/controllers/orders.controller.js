const { Orders, ProductsEN } = require("../models/index");

module.exports = {
    
  getAllOrders: async (req, res) => {
    return await Orders.findAll()
      .then((order) => res.json(order))
      .catch((err) => console.log(err));
  },

  getOrder: async (req, res) => {
    const { id } = req.params;
    return await Orders.findOne({
      where: { orderId: id },
    })
      .then((order) => res.json(order))
      .catch((err) => console.log(err));
  },

  createOrder: async (req, res) => {
    const { products, userId, orderSum, notes, status } = req.body;
    return await Orders.create({
      products,
      userId,
      orderSum,
      notes,
      status,
    })
      .then((order) => res.send(req.body))
      .catch((err) => res.send(err));
  },

  updateOrder: async (req, res) => {
    const { id } = req.params;
    const { products, userId, orderSum, notes, status } = req.body;
    return await Orders.update(
      { products, userId, orderSum, notes, status },
      { where: { orderId: id } }
    )
      .then((order) => res.json(order))
      .catch((err) => console.log(err));
  },

  deleteOrder: async (req, res) => {
    const { id } = req.params;
    return await Orders.destroy({ where: { orderId: id } })
      .then((order) => res.json(order))
      .catch((err) => console.log(err));
  },
  //   restoreOrder: async (req, res) => {
  //     const { id } = req.params;
  //     return await Orders.update({ deletedAt: null }, { where: { orderId: id } })
  //       .then((order) => res.json(order))
  //       .catch((err) => console.log(err));
  //   },

  //   softDeleteOrder: async (req, res) => {
  //     const { id } = req.params;
  //     return await Orders.update(
  //       { deletedAt: "now()" },
  //       { where: { orderId: id } }
  //     )
  //       .then((order) => res.json(order))
  //       .catch((err) => console.log(err));
  //   },
};
