const { ProductsEN } = require("../models/index");
const jwt = require("jsonwebtoken");

module.exports = {
  getAllProducts: async (req, res) => {
    return await ProductsEN.findAll()
      .then((products) => res.json(products))
      .catch((err) => console.log(err));
  },

  getProduct: async (req, res) => {
    const { id } = req.params;
    return await ProductsEN.findOne({ where: { productId: id } })
      .then((product) => res.json(product))
      .catch((err) => console.log(err));
  },

  createProduct: async (req, res) => {
    const {
      title,
      description,
      price,
      mainImg,
      categoryId,
      keywords,
    } = req.body;
    return await ProductsEN.create({
      title,
      description,
      price,
      mainImg,
      categoryId,
      keywords,
    })
      .then((product) => {
        const bearerToken = req.headers.authorization;
        if (bearerToken) {
          const token = bearerToken.split(" ")[1];
          jwt.verify(token, process.env.SECRET_MOD, (err, productId) => {
            console.log("err = ", err);
            if (err) return res.sendStatus(403);
            // next();
            res.json(product);
          });
        } else {
          res.sendStatus(401);
        }
      })
      .catch((err) => console.log(err));
  },

  updateProduct: async (req, res) => {
    const { id } = req.params;
    const {
      title,
      description,
      price,
      mainImg,
      categoryId,
      keywords,
    } = req.body;
    return await ProductsEN.update(
      { title, description, price, mainImg, categoryId, keywords },
      { where: { productId: id } }
    )
      .then((product) => {
        res.json(product);
      })
      .catch((err) => console.log(err));
  },

  restoreProduct: async (req, res) => {
    if (req.session.loggedIn) {
      const { id } = req.params;
      return await ProductsEN.update(
        { deletedAt: null },
        { where: { productId: id } }
      )
        .then((product) => {
          const bearerToken = req.headers.authorization;
          if (bearerToken) {
            const token = bearerToken.split(" ")[1];
            jwt.verify(token, process.env.SECRET_MOD, (err, productId) => {
              console.log("err = ", err);
              if (err) return res.sendStatus(403);
              // next();
              res.json(product);
            });
          } else {
            res.sendStatus(401);
          }
        })
        .catch((err) => console.log(err));
    } else {
      res.json({ isAdmin: false });
    }
  },

  softDeleteProduct: async (req, res) => {
    if (req.session.loggedIn) {
      const { id } = req.params;
      return await ProductsEN.update(
        { deletedAt: "now()" },
        { where: { productId: id } }
      )
        .then((product) => {
          const bearerToken = req.headers.authorization;
          if (bearerToken) {
            const token = bearerToken.split(" ")[1];
            jwt.verify(token, process.env.SECRET_MOD, (err, productId) => {
              console.log("err = ", err);
              if (err) return res.sendStatus(403);
              // next();
              res.json(product);
            });
          } else {
            res.sendStatus(401);
          }
        })
        .catch((err) => console.log(err));
    } else {
      res.json({ isAdmin: false });
    }
  },

  deleteProduct: async (req, res) => {
    const { id } = req.params;
    return await ProductsEN.destroy({ where: { productId: id } }).then(
      (product) => {
        res.json(product);
      }
    );
  },
};
