const { User, Roles } = require("../models/index");
const crypto = require("crypto");
const main = require("../mailer/mailer");
const jwt = require("jsonwebtoken");

module.exports = {
  getAllUsers: async (req, res) => {
    return await User.findAll()
      .then((user) => res.json(user))
      .catch((err) => console.log(err));
  },

  getUser: async (req, res, next) => {
    const { id } = req.params;
    return await User.findOne({
      where: { userId: id },
      include: Roles,
    })
      .then((user) => {
        res.json(user);
      })
      .catch((err) => err && res.sendStatus(409));
  },

  login: async (req, res) => {
    const { email, password } = req.body;
    const key = crypto.createHash("md5").update(password).digest("hex");
    return await User.findOne({ where: { email: email, password: key } })
      .then((user) => {
        if (user.email === email) {
          if (user.password === key) {
            if (user.roleId === 1) {
              const token = jwt.sign(
                { id: user.userId },
                process.env.SECRET_MOD,
                {
                  expiresIn: "1h",
                }
              );
              res.json({ token, isAdmin: user.roleId });
            } else {
              const token = jwt.sign({ id: user.userId }, process.env.SECRET, {
                expiresIn: "1h",
              });
              res.send({ token, user });
            }
          } else {
            res.sendStatus(403);
          }
        } else {
          res.sendStatus(403);
        }
      })
      .catch((err) => err && res.sendStatus(403));
  },

  logout: async (req, res) => {
    res.header("authorization", null);
    res.send("logout");
  },

  register: async (req, res) => {
    const { firstName, lastName, phone, email, password, roleId } = req.body;
    const key = crypto.createHash("md5").update(password).digest("hex");
    return await User.findOne({ where: { email: email } }).then((user) => {
      if (!user) {
        User.create({
          firstName,
          lastName,
          phone,
          email,
          password: key,
          roleId,
        })
          .then((user) => {
            const token = jwt.sign({ id: user.userId }, process.env.SECRET, {
              expiresIn: "1h",
            });
            res.send({ token, user });
            main();
          })
          .catch((err) => err && res.sendStatus(409));
      } else {
        res.sendStatus(409);
      }
    });
  },

  updateUser: async (req, res) => {
    const { id } = req.params;
    const { firstName, lastName, phone, email, password, roleId } = req.body;
    const key = crypto.createHash("md5").update(password).digest("hex");
    return await User.update(
      {
        firstName: firstName,
        lastName: lastName,
        phone: phone,
        email: email,
        key: key,
        roleId: roleId,
      },
      { where: { userId: id } }
    )
      .then((user) => res.json(user))
      .catch((err) => err && res.sendStatus(409));
  },

  restoreUser: async (req, res) => {
    const { id } = req.params;
    return await User.update({ deletedAt: null }, { where: { userId: id } })
      .then((user) => res.json(user))
      .catch((err) => res.send(err));
  },

  softDeleteUser: async (req, res) => {
    const { id } = req.params;
    return await User.update({ deletedAt: "now()" }, { where: { userId: id } })
      .then((user) => res.json(user))
      .catch((err) => res.send(err));
  },

  deleteUser: async (req, res) => {
    const { id } = req.params;
    return await User.destroy({ where: { userId: id } })
      .then((user) => res.json(user))
      .catch((err) => res.send(err));
  },
};
