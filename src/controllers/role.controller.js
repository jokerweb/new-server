const { Roles } = require("../models/index");

module.exports = {
  getAllRoles: async (req, res) => {
    return await Roles.findAll()
      .then((roles) => res.json(roles))
      .catch((err) => console.log(err));
  },

  getRole: async (req, res) => {
    const { id } = req.params;
    return await Roles.findOne({ where: { roleId: id } })
      .then((role) => res.json(role))
      .catch((err) => console.log(err));
  },

  createRole: async (req, res) => {
    const { roleName, rolePriority } = req.body;
    return await Roles.create({ roleName, rolePriority })
      .then((role) => res.json(role))
      .catch((err) => console.log(err));
  },

  deleteRole: async (req, res) => {
    const { id } = req.params;
    return await Roles.destroy({ where: { roleId: id } })
      .then((role) => res.json(role))
      .catch((err) => console.log(err));
  },
};
