const { Pilot } = require("../models/index");

module.exports = {
  getAllPilot: async (req, res) => {
    return await Pilot.findAll()
      .then((pilot) => res.json(pilot))
      .catch((err) => res.send(err));
  }
};
