const app = require("express").Router();
const {
  getAllOrders,
  getOrder,
  createOrder,
  updateOrder,
  deleteOrder,
} = require("../controllers/orders.controller");

const { verifyAdminToken, verifyUserToken } = require("../middleware/JWTauth");

app.get("/orders", verifyAdminToken, getAllOrders);

app.get("/order/:id", verifyAdminToken, getOrder);

app.post("/order/create", verifyUserToken, createOrder);

app.patch("/order/update/:id", verifyAdminToken, updateOrder);

app.delete("/order/delete/:id", verifyAdminToken, deleteOrder);

module.exports = app;
