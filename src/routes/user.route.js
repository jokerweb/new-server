const app = require("express").Router();
const {
  getAllUsers,
  getUser,
  login,
  register,
  updateUser,
  deleteUser,
  restoreUser,
  softDeleteUser,
  logout,
} = require("../controllers/user.controller");

const { verifyUserToken, verifyAdminToken } = require("../middleware/JWTauth");

app.get("/users", getAllUsers);

app.get("/profile/:id", verifyUserToken, getUser);

app.post("/user", login);

app.post("/logout", logout);

app.post("/user/create", register);

app.patch("/user/update/:id", verifyUserToken, updateUser);

app.patch("/user/soft-delete/:id", verifyAdminToken, softDeleteUser);

app.patch("/user/restore/:id", verifyAdminToken, restoreUser);

app.delete("/user/delete/:id", verifyAdminToken, deleteUser);

module.exports = app;
