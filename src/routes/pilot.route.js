const app = require("express").Router();
const { getAllPilot } = require("../controllers/pilot.controller");

app.post("/pilot", getAllPilot);

module.exports = app;
