const app = require("express").Router();
const {
  getAllProducts,
  getProduct,
  createProduct,
  updateProduct,
  restoreProduct,
  softDeleteProduct,
  deleteProduct,
} = require("../controllers/products.controller");

const { verifyAdminToken } = require("../middleware/JWTauth");

app.get("/products/:lang", getAllProducts);

app.get("/product/:id", getProduct);

app.post("/product/create", verifyAdminToken, createProduct);

app.patch("/product/update/:id", verifyAdminToken, updateProduct);

app.patch("/product/restore/:id", verifyAdminToken, restoreProduct);

app.patch("/product/soft-delete/:id", verifyAdminToken, softDeleteProduct);

app.delete("/product/delete/:id", verifyAdminToken, deleteProduct);

module.exports = app;
