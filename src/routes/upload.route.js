const app = require("express").Router();
const {
  mainImgUpload,
  galleryUpload,
} = require("../controllers/upload.controller");
const { verifyAdminToken } = require("../middleware/JWTauth");
const multer = require("multer");

const fs = require("fs");

const path = "public/assets/products/";
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (fs.existsSync(path + req.params.id)) {
      null;
    } else fs.mkdirSync(path + req.params.id);
    cb(null, path + req.params.id);
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage: storage });

app.post("/product/:id/upload", mainImgUpload);

// app.post("/product/:id/gallery", galleryUpload);
app.post("/product/:id/gallery", upload.array("gallery", 10), galleryUpload);

module.exports = app;
