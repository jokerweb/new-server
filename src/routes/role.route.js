const app = require("express").Router();
const {
  getAllRoles,
  getRole,
  createRole,
  deleteRole,
} = require("../controllers/role.controller");

const { verifyAdminToken } = require("../middleware/JWTauth");

app.get("/roles", verifyAdminToken, getAllRoles);

app.get("/role/:id", verifyAdminToken, getRole);

app.post("/role/create", verifyAdminToken, createRole);

app.delete("/role/delete/:id", verifyAdminToken, deleteRole);

module.exports = app;
